# README #

«Analyzer» - графическое приложение на платформе Java, реализующее функционал для построения графиков на основе данных, 
получаемых в виде excel-таблиц от датчиков вибрации системы диагностики и сохраненных на сервере Wialon Local НТЦК ОАО
"Гомсельмаш".

### Запуск приложения ###

Приложение запускается java AnalyzerApp, после чего загружается главный экран приложения.

### Использованные библиотеки ###
Были использованы следующие библиотеки:
* Java 8 + JavaFX;
* Gradle 7.5;
* Lombok;
* Apache POI - Java API To Access Microsoft Format Files;
* JFXUtils - Zoom and Pan Charts and Pane Scaling.

### Примечания ###
* Более подробная информация о работе приложения: [User_manual](manual/user_manual_analyzer.pdf).

