package by.ntck.koaims.data;

import java.util.List;

/**
 * Интерфейс чтения данных.
 *
 * @author Ловцов Алексей
 */
public interface DataReader {

    /**
     * Получить строки с данными датчиков комбайна.
     *
     * @return строки с данными
     */
    List<String> getSensors();
}
