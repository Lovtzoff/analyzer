package by.ntck.koaims.data;

import by.ntck.koaims.model.Message;

import java.util.List;

/**
 * Интерфейс чтения книг Excel.
 *
 * @author Ловцов Алексей
 */
public interface ExcelReader {

    /**
     * Прочитать список данных из книги Excel.
     *
     * @return список данных
     */
    List<Message> readExcel();

    /**
     * Прочитать тип GPS-устройства из книги Excel.
     */
    void readGpsDeviceType();

    /**
     * Прочитать модель комбайна из книги Excel.
     */
    void readHarvesterModel();
}
