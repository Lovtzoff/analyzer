package by.ntck.koaims.data.impl;

import by.ntck.koaims.data.DataReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static by.ntck.koaims.constants.Constants.SENSOR_LIST;
import static by.ntck.koaims.constants.Constants.harvesterModel;

/**
 * Реализация интерфейса DataReader.
 *
 * @author Ловцов Алексей
 * @see DataReader
 */
public class DataReaderImpl implements DataReader {

    @Override
    public List<String> getSensors() {
        return readAllLines();
    }

    /**
     * Прочитать все строки.
     *
     * @return строки данных
     */
    private List<String> readAllLines() {
        try {
            List<String> strings = Files.readAllLines(Paths.get(SENSOR_LIST));
            strings.removeIf(string -> string.equals(""));
            return strings.stream()
                    .filter(string -> !string.startsWith("#"))
                    .filter(string -> string.contains(harvesterModel))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
