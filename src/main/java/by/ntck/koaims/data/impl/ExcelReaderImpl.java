package by.ntck.koaims.data.impl;

import by.ntck.koaims.data.ExcelReader;
import by.ntck.koaims.model.DiagnosticData;
import by.ntck.koaims.model.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static by.ntck.koaims.constants.Constants.BlockGps.SMART;
import static by.ntck.koaims.constants.Constants.BlockGps.VEGA;
import static by.ntck.koaims.constants.Constants.*;
import static org.apache.poi.ss.usermodel.CellType.BLANK;
import static org.apache.poi.ss.usermodel.CellType.STRING;

/**
 * Реализация интерфейса ExcelReader.
 *
 * @author Ловцов Алексей
 * @see ExcelReader
 */
@NoArgsConstructor
@AllArgsConstructor
public class ExcelReaderImpl implements ExcelReader {

    private static int index;
    private static String dateTime;
    private static int byte0;
    private static int byte1;
    private static int byte2;
    private static int byte3;
    private static int byte4;
    private static int byte5;
    private static int byte6;
    private static int byte7;

    /**
     * Путь к книге Excel.
     */
    @Getter
    @Setter
    private String excelPath;
    /**
     * Имя листа для чтения.
     */
    @Getter
    @Setter
    private String sheetName;

    @Override
    public List<Message> readExcel() {
        List<Message> messageList = new ArrayList<>();

        try {
            XSSFWorkbook excelBook = new XSSFWorkbook(Files.newInputStream(Paths.get(excelPath)));
            XSSFSheet excelSheet = excelBook.getSheet(sheetName);

            if (excelSheet != null) {
                replaceDashWithZero(excelBook, excelSheet);
                // Перебираем все строки в книге
                for (Row row : excelSheet) {
                    // Кроме строки с заголовками + проверка ячеек нулевого столбца на наличие значения
                    if ((row.getRowNum() != 0) && !isCellEmpty(row.getCell(0))) {
                        // Перебираем все ячейки в строке
                        for (Cell cell : row) {
                            // Получить тип ячейки
                            switch (cell.getCellType()) {
                                case STRING:
                                    if (cell.getColumnIndex() == 0) {
                                        index = Integer.parseInt(cell.getStringCellValue());
                                    } else if (cell.getColumnIndex() == 1) {
                                        dateTime = cell.getStringCellValue();
                                    }
                                    break;
                                case NUMERIC:
                                    if (cell.getColumnIndex() == 2) {
                                        byte0 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 3) {
                                        byte1 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 4) {
                                        byte2 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 5) {
                                        byte3 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 6) {
                                        byte4 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 7) {
                                        byte5 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 8) {
                                        byte6 = (int) cell.getNumericCellValue();
                                    } else if (cell.getColumnIndex() == 9) {
                                        byte7 = (int) cell.getNumericCellValue();
                                    }
                                    break;
                            }
                        }
                        messageList.add(new Message(
                                index,
                                dateTime,
                                new DiagnosticData(
                                        byte0,
                                        byte1,
                                        byte2,
                                        byte3,
                                        byte4,
                                        byte5,
                                        byte6,
                                        byte7
                                )));
                    }
                }
            }
            excelBook.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return messageList;
    }

    @Override
    public void readGpsDeviceType() {
        try {
            XSSFWorkbook excelBook = new XSSFWorkbook(Files.newInputStream(Paths.get(excelPath)));
            XSSFSheet excelSheet = excelBook.getSheet(SHEET_NAME_DEVICE);

            // Перебираем все строки в книге
            for (Row row : excelSheet) {
                // Кроме строки с заголовками + проверка ячеек нулевого столбца на наличие значения
                if ((row.getRowNum() != 0) && !isCellEmpty(row.getCell(0))) {
                    // Перебираем все ячейки в строке
                    for (Cell cell : row) {
                        // Получить тип ячейки
                        switch (cell.getCellType()) {
                            case STRING:
                                if (cell.getColumnIndex() == 1) {
                                    String deviceType = cell.getStringCellValue();
                                    if (deviceType.contains(VEGA.toString())) {
                                        currentBlockGps = VEGA;
                                    } else if (deviceType.contains(SMART.toString())) {
                                        currentBlockGps = SMART;
                                    }
                                }
                                break;
                            case NUMERIC:
                                break;
                        }
                    }
                }
            }
            excelBook.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void readHarvesterModel() {
        try {
            XSSFWorkbook excelBook = new XSSFWorkbook(Files.newInputStream(Paths.get(excelPath)));
            XSSFSheet excelSheet = excelBook.getSheet(SHEET_OBJECT_CHARACTERISTICS);

            // Перебираем все строки в книге
            for (Row row : excelSheet) {
                // 1 строка с маркой комбайна + проверка ячеек нулевого столбца на наличие значения
                if ((row.getRowNum() == 1) && !isCellEmpty(row.getCell(0))) {
                    // Перебираем все ячейки в строке
                    for (Cell cell : row) {
                        // Получить тип ячейки
                        switch (cell.getCellType()) {
                            case STRING:
                                if (cell.getColumnIndex() == 1) {
                                    harvesterModel = cell.getStringCellValue();
                                }
                                break;
                            case NUMERIC:
                                break;
                        }
                    }
                }
            }
            excelBook.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Проверяет, является ли значение данной ячейки {@link Cell} пустым.
     *
     * @param cell Ячейка для проверки {@link Cell}.
     * @return {@code true}, если ячейка {@link Cell} пуста. {@code false} в противном случае.
     */
    private static boolean isCellEmpty(final Cell cell) {
        if (cell == null) { // use row.getCell(x, Row.CREATE_NULL_AS_BLANK) to avoid null cells
            return true;
        }

        if (cell.getCellType() == BLANK) {
            return true;
        }

        return (cell.getCellType() == STRING) && (cell.getStringCellValue().trim().isEmpty());
    }

    /**
     * Найти и заменить прочерк на ноль.
     *
     * @param excelBook  the Excel book
     * @param excelSheet the Excel sheet
     */
    private void replaceDashWithZero(XSSFWorkbook excelBook, XSSFSheet excelSheet) {
        boolean containsDash = false;
        // пройтись по всем рядам
        for (Row row : excelSheet) {
            // пройтись по всем ячейкам
            for (Cell cell : row) {
                // проверить тип ячейки
                if (cell.getCellType() == STRING) {
                    // получить значение ячейки в виде строки
                    String value = cell.getStringCellValue();
                    // проверить, равно ли значение "-----" (прочерку)
                    if (value.equals("-----")) {
                        containsDash = true;
                        // установить значение ячейки на 0
                        cell.setCellValue(0);
                    }
                }
            }
        }
        if (containsDash) {
            // сохранить книгу
            try {
                excelBook.write(Files.newOutputStream(Paths.get(excelPath)));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
