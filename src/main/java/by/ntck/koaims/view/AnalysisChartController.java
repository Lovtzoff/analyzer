package by.ntck.koaims.view;

import by.ntck.koaims.AnalyzerApp;
import by.ntck.koaims.data.DataReader;
import by.ntck.koaims.data.impl.DataReaderImpl;
import by.ntck.koaims.data.impl.ExcelReaderImpl;
import by.ntck.koaims.exception.InvalidInputDataException;
import by.ntck.koaims.model.*;
import by.ntck.koaims.service.DataPackageService;
import by.ntck.koaims.service.SensorService;
import by.ntck.koaims.service.impl.ChannelServiceImpl;
import by.ntck.koaims.service.impl.DataPackageServiceImpl;
import by.ntck.koaims.service.impl.SensorServiceImpl;
import by.ntck.koaims.util.CustomCellUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;
import org.gillius.jfxutils.chart.ChartPanManager;
import org.gillius.jfxutils.chart.JFXChartUtil;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static by.ntck.koaims.constants.Constants.*;

/**
 * Класс-контроллер для представления макета <b>AnalysisChartView.fxml</b>.
 *
 * @author Ловцов Алексей
 */
public class AnalysisChartController {

    @FXML
    private Button btOpenFile;
    @FXML
    private Button btNextPackage;
    @FXML
    private Button btPreviousPackage;
    @FXML
    private Button btClearChart;
    @FXML
    private CheckBox cbSetSymbols;
    @FXML
    private ToggleGroup blocks;
    @FXML
    private RadioButton rbBlock1;
    @FXML
    private RadioButton rbBlock2;
    @FXML
    private Label gpsDeviceLabel;
    @FXML
    private Label lbHarvester;

    @FXML
    private LineChart<Number, Number> chartFromFileData;
    @FXML
    private TabPane analysisTabPane;
    @FXML
    private Tab tablesTab;
    @FXML
    private TableView<ChartPoint> chartPointsTableView;
    @FXML
    private Label lbXAxis;
    @FXML
    private TableColumn<ChartPoint, Double> frequencyColumn;
    @FXML
    private TableColumn<ChartPoint, Double> amplitudeColumn;
    @FXML
    private TableView<ChartPoint> yAxisChartPointsTableView;
    @FXML
    private Label lbYAxis;
    @FXML
    private TableColumn<ChartPoint, Double> yAxisFrequencyColumn;
    @FXML
    private TableColumn<ChartPoint, Double> yAxisAmplitudeColumn;
    @FXML
    private TableView<ChartPoint> zAxisChartPointsTableView;
    @FXML
    private Label lbZAxis;
    @FXML
    private TableColumn<ChartPoint, Double> zAxisFrequencyColumn;
    @FXML
    private TableColumn<ChartPoint, Double> zAxisAmplitudeColumn;

    @FXML
    private Label lbNumberPackage;
    @FXML
    private Label lbPackageNumber;
    @FXML
    private Label lbTimestamp;
    @FXML
    private Label lbSensor;
    @FXML
    private Label lbChannelNumber;
    @FXML
    private Label lbSampleFrequency;
    @FXML
    private Label lbNumberSamples;
    @FXML
    private Label lbNumberBins;
    @FXML
    private Label lbSensorRange;
    @FXML
    private Label lbSensorSensitivity;
    @FXML
    private Label lbAdcBit;
    @FXML
    private Label lbRmsValue;
    @FXML
    private Label lbMaxValue;
    @FXML
    private Label lbConstComponent;
    @FXML
    private Label lbEngineSpeed;
    @FXML
    private Label lbEngineLoading;
    @FXML
    private TabPane optionsTabPane;
    @FXML
    private Tab channelsTab;
    @FXML
    private ComboBox<String> channelsComboBox;
    @FXML
    private ListView<String> timestampsListView;
    @FXML
    private Tab sensorsTab;
    @FXML
    private ComboBox<String> sensorsComboBox;
    @FXML
    private Label lbPackagesNumberForXAxis;
    @FXML
    private Label lbPackagesNumberForYAxis;
    @FXML
    private Label lbPackagesNumberForZAxis;
    @FXML
    private Label lbCurrentIndex;
    @FXML
    private CheckBox cbOnOffXAxis;
    @FXML
    private CheckBox cbOnOffYAxis;
    @FXML
    private CheckBox cbOnOffZAxis;

    @FXML
    private Label lbFrequency;
    @FXML
    private Label lbAmplitude;

    // Ссылка на главное приложение.
    private AnalyzerApp analyzerApp;

    private File file;

    private final ExcelReaderImpl excelReader = new ExcelReaderImpl();
    private final DataPackageService dataPackageService = new DataPackageServiceImpl(excelReader);
    private final DataReader dataReader = new DataReaderImpl();
    private final SensorService sensorService = new SensorServiceImpl(dataReader, dataPackageService);
    private List<DataPackage> packageList = new ArrayList<>();
    private List<Channel> channelList = new ArrayList<>();
    private List<Sensor> sensorList = new ArrayList<>();
    private int currentIndex = 0;
    private double ratioSampleFreqToNumberSamples = 0;
    private float constComponent = 0;
    private final ObservableList<ChartPoint> chartPointDataList = FXCollections.observableArrayList();
    private final ObservableList<ChartPoint> yAxisChartPointDataList = FXCollections.observableArrayList();
    private final ObservableList<ChartPoint> zAxisChartPointDataList = FXCollections.observableArrayList();

    /**
     * Конструктор.
     * Конструктор вызывается раньше метода initialize().
     */
    public AnalysisChartController() {
    }

    /**
     * Инициализация класса-контроллера. Этот метод вызывается автоматически
     * после того, как fxml-файл будет загружен.
     */
    @FXML
    private void initialize() {
        chartFromFileData.setCreateSymbols(false);
        chartFromFileData.setLegendVisible(false);

        // Блок по умолчанию
        diagnosticsBlock = BLOCK_A1;
        excelReader.setSheetName(SHEET_NAME_FOR_FIRST_BLOCK);

        initializeChartControlWithMouse();

        initializeDestinationTables();

        // TabPane
        optionsTabPane
                .getSelectionModel()
                .selectedIndexProperty()
                .addListener((observable, oldTab, newTab) -> {
                    if (!newTab.equals(oldTab)) {
                        currentIndex = 0;
                        chartFromFileData.getData().clear();
                        chartFromFileData.setLegendVisible(false);
                        lbTimestamp.setText(null);
                        lbSensor.setText(null);
                        channelsComboBox.setValue(channelsComboBox.getPromptText());
                        sensorsComboBox.setValue(sensorsComboBox.getPromptText());
                        timestampsListView.setItems(FXCollections.observableArrayList());
                        yAxisChartPointsTableView.setDisable(true);
                        zAxisChartPointsTableView.setDisable(true);
                        lbXAxis.setDisable(true);
                        lbYAxis.setDisable(true);
                        lbZAxis.setDisable(true);
                    }
                });

        // channelsComboBox
        channelsComboBox
                .getSelectionModel()
                .selectedIndexProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (!newValue.equals(oldValue)) {
                        chartFromFileData.getData().clear();
                        lbSensor.setText(null);
                    }
                });
        // timestampsListView
        timestampsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // Обработчик выбора метки времени на вкладке "Каналы".
        timestampsListView
                .getSelectionModel()
                .selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        drawChartForChannelTimestamp(Integer.parseInt(channelsComboBox.getValue()), newValue);
                    }
                });
    }

    /**
     * Инициализация управления графиком с помощью мыши.
     */
    private void initializeChartControlWithMouse() {
        // Подсветка данных по движению мыши
        chartFromFileData.setOnMouseMoved(mouseEvent -> {
            double xStart = chartFromFileData.getXAxis().getLocalToParentTransform().getTx();
            double yStart = chartFromFileData.getYAxis().getLocalToParentTransform().getTx();
            double axisXRelativeMousePosition = mouseEvent.getX() - xStart;
            double axisYRelativeMousePosition = mouseEvent.getY() - yStart;
            double frequency =
                    chartFromFileData.getXAxis().getValueForDisplay(axisXRelativeMousePosition).doubleValue();
            double amplitude =
                    chartFromFileData.getYAxis().getValueForDisplay(axisYRelativeMousePosition).doubleValue();
            lbFrequency.setText(String.format("%.3f", frequency));
            lbAmplitude.setText(String.format("%.2f", amplitude));
        });

        // Панорамирование работает либо с помощью вторичной (правой) мыши, либо с помощью основной с зажатым Ctrl
        ChartPanManager panManager = new ChartPanManager(chartFromFileData);
        panManager.setMouseFilter(mouseEvent -> {
            if ((mouseEvent.getButton() == MouseButton.SECONDARY) ||
                    (mouseEvent.getButton() == MouseButton.PRIMARY && mouseEvent.isShortcutDown())) {
            } else {
                mouseEvent.consume();
            }
        });
        panManager.start();

        // Масштабирование работает только с помощью основной кнопки мыши без зажатого ctrl
        JFXChartUtil.setupZooming(chartFromFileData, mouseEvent -> {
            if ((mouseEvent.getButton() != MouseButton.PRIMARY) || mouseEvent.isShortcutDown()) {
                mouseEvent.consume();
            }
        });

        // Возврат исходного графика по двойному щелчку
        JFXChartUtil.addDoublePrimaryClickAutoRangeHandler(chartFromFileData);
    }

    /**
     * Initialize destination tables.
     * Инициализировать таблицы адресатов.
     */
    private void initializeDestinationTables() {
        frequencyColumn.setCellValueFactory(new PropertyValueFactory<>("frequency"));
        amplitudeColumn.setCellValueFactory(new PropertyValueFactory<>("amplitude"));

        yAxisFrequencyColumn.setCellValueFactory(new PropertyValueFactory<>("frequency"));
        yAxisAmplitudeColumn.setCellValueFactory(new PropertyValueFactory<>("amplitude"));

        zAxisFrequencyColumn.setCellValueFactory(new PropertyValueFactory<>("frequency"));
        zAxisAmplitudeColumn.setCellValueFactory(new PropertyValueFactory<>("amplitude"));

        NumberFormat format = NumberFormat.getNumberInstance();
        amplitudeColumn.setCellFactory(column -> new CustomCellUtil(format));
        yAxisAmplitudeColumn.setCellFactory(column -> new CustomCellUtil(format));
        zAxisAmplitudeColumn.setCellFactory(column -> new CustomCellUtil(format));
    }

    /**
     * Открывает FileChooser, чтобы позволить пользователю выбрать книгу для загрузки данных.
     *
     * @throws IOException the io exception
     */
    @FXML
    private void handleOpen() throws IOException {
        File dataDir = new File(DATA_DIR);
        Files.createDirectories(dataDir.toPath());
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open file");
        fileChooser.setInitialDirectory(dataDir);

        // Установить фильтр расширения
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Files", "*.*"),
                new FileChooser.ExtensionFilter("XLSX Files", "*.xlsx")
        );

        // Показать диалоговое окно открытия файла
        File selectedFile = fileChooser.showOpenDialog(analyzerApp.getPrimaryStage());

        if (selectedFile != null) {
            chartFromFileData.getData().clear();

            if (!selectedFile.getParent().equals(dataDir.getAbsolutePath())) {
                FileUtils.copyFileToDirectory(selectedFile, dataDir);
                file = FileUtils.getFile(dataDir, selectedFile.getName());
            } else {
                file = selectedFile;
            }

            excelReader.setExcelPath(file.getAbsolutePath());
            excelReader.readGpsDeviceType();
            excelReader.readHarvesterModel();
            gpsDeviceLabel.setText(currentBlockGps.toString());
            lbHarvester.setText(harvesterModel);
            getDataSelectedFile();
        }
    }

    /**
     * Переходит к следующему пакету.
     */
    @FXML
    private void handleNextPackage() {
        if (excelReader.getExcelPath() != null) {
            chartFromFileData.getData().clear();
            currentIndex++;
            if (sensorsTab.isSelected()) {
                int size = Integer.parseInt(lbPackagesNumberForXAxis.getText());
                currentIndex = !(currentIndex < size) ? 0 : currentIndex;
                handleSelectSensor();
            } else {
                currentIndex = !(currentIndex < packageList.size()) ? 0 : currentIndex;
                showDataOnePackage(currentIndex);
            }
        }
    }

    /**
     * Возвращается к предыдущему пакету.
     */
    @FXML
    private void handlePreviousPackage() {
        if (excelReader.getExcelPath() != null) {
            chartFromFileData.getData().clear();
            currentIndex--;
            if (sensorsTab.isSelected()) {
                int xAxisSize = Integer.parseInt(lbPackagesNumberForXAxis.getText());
                int yAxisSize = Integer.parseInt(lbPackagesNumberForYAxis.getText());
                int zAxisSize = Integer.parseInt(lbPackagesNumberForZAxis.getText());
                int size = Stream.of(xAxisSize, yAxisSize, zAxisSize).min(Integer::compareTo).get();
                currentIndex = !(currentIndex >= 0) ? (size - 1) : currentIndex;
                handleSelectSensor();
            } else {
                currentIndex = !(currentIndex >= 0) ? (packageList.size() - 1) : currentIndex;
                showDataOnePackage(currentIndex);
            }
        }
    }

    /**
     * Очищает текущий график.
     */
    @FXML
    private void handleClearChart() {
        chartFromFileData.getData().clear();
        chartPointDataList.clear();
        yAxisChartPointDataList.clear();
        zAxisChartPointDataList.clear();
        lbTimestamp.setText(null);
        lbSensor.setText(null);
    }

    /**
     * Включает и отключает отображение символов и всплывающих подсказок.
     */
    @FXML
    private void handleSetSymbols() {
        chartFromFileData.setCreateSymbols(cbSetSymbols.isSelected());
        if (chartFromFileData.getCreateSymbols()) {
            // Просмотр данных и применение всплывающей подсказки
            for (XYChart.Series<Number, Number> series : chartFromFileData.getData()) {
                for (XYChart.Data<Number, Number> data : series.getData()) {
                    Tooltip.install(data.getNode(), new Tooltip(
                                    "Частота, Гц: " + String.format("%.3f", data.getXValue().doubleValue()) + "\n" +
                                            "Амплитуда, g: " + String.format("%.2f", data.getYValue().doubleValue())
                            )
                    );

                    // Добавление класса при наведении
                    data.getNode().setOnMouseEntered(
                            mouseEvent -> data.getNode().getStyleClass().add("onHover")
                    );

                    // Удаление класса при выходе
                    data.getNode().setOnMouseExited(
                            mouseEvent -> data.getNode().getStyleClass().remove("onHover")
                    );
                }
            }
        }
    }

    /**
     * Выбор блока, для которого нужно обработать данные.
     */
    @FXML
    private void handleSelectBlock() {
        if (excelReader.getExcelPath() != null) {
            chartFromFileData.getData().clear();

            if (rbBlock1.isSelected()) {
                diagnosticsBlock = BLOCK_A1;
                excelReader.setSheetName(SHEET_NAME_FOR_FIRST_BLOCK);
            } else if (rbBlock2.isSelected()) {
                diagnosticsBlock = BLOCK_A2;
                excelReader.setSheetName(SHEET_NAME_FOR_SECOND_BLOCK);
            }

            getDataSelectedFile();
        }
    }

    /**
     * Выбор вкладки "Таблицы".
     */
    @FXML
    private void handleTablesTabSelect() {
        if (tablesTab.isSelected()) {
            chartPointsTableView.setItems(chartPointDataList);
            if (!sensorsTab.isSelected()) {
                yAxisChartPointsTableView.setDisable(true);
                zAxisChartPointsTableView.setDisable(true);
                lbXAxis.setDisable(true);
                lbYAxis.setDisable(true);
                lbZAxis.setDisable(true);
            }
        }
    }

    /**
     * Выбор вкладки "Каналы".
     */
    @FXML
    private void handleChannelsTabSelect() {
        if (channelsTab.isSelected()) {
            ObservableList<String> channels = FXCollections.observableArrayList();
            channelList.forEach(channel -> channels.add(String.valueOf(channel.getChannelNumber())));
            channelsComboBox.setItems(channels);
        }
    }

    /**
     * Обработчик выбора канала.
     */
    @FXML
    private void handleSelectChannel() {
        String selectedChannel = channelsComboBox.getValue();
        if (selectedChannel == null || selectedChannel.equals(channelsComboBox.getPromptText())) {
            channelsComboBox.getSelectionModel().clearSelection();
        } else {
            ObservableList<String> timestamps = FXCollections.observableArrayList(
                    channelList.get(Integer.parseInt(selectedChannel)).getTimestamp()
            );
            timestampsListView.setItems(timestamps);
        }
    }

    /**
     * Выбор вкладки "Датчики".
     */
    @FXML
    private void handleSensorsTabSelect() {
        if (sensorsTab.isSelected()) {
            ObservableList<String> sensors = FXCollections.observableArrayList();
            sensorList.forEach(sensor -> sensors.add(sensor.getName()));
            sensorsComboBox.setItems(sensors);

            yAxisChartPointsTableView.setDisable(false);
            yAxisChartPointsTableView.setItems(yAxisChartPointDataList);
            zAxisChartPointsTableView.setDisable(false);
            zAxisChartPointsTableView.setItems(zAxisChartPointDataList);

            lbXAxis.setDisable(false);
            lbYAxis.setDisable(false);
            lbZAxis.setDisable(false);
        }
    }

    /**
     * Обработчик выбора датчика.
     */
    @FXML
    private void handleSelectSensor() {
        chartFromFileData.getData().clear();

        chartPointDataList.clear();
        yAxisChartPointDataList.clear();
        zAxisChartPointDataList.clear();

        String selectedSensor = sensorsComboBox.getValue();
        if (selectedSensor == null || selectedSensor.equals(sensorsComboBox.getPromptText())) {
            sensorsComboBox.getSelectionModel().clearSelection();
        } else {
            Sensor sensor = sensorService.findOneByName(selectedSensor);
            drawChartForSensorByIndex(sensor);
            lbTimestamp.setText("Время получения данных: " +
                    sensor.getXAxisData()
                            .get(currentIndex)
                            .getPackageHeaderData()
                            .getDateTime());
        }
    }

    /**
     * Обработчик Вкл/Выкл оси x.
     */
    @FXML
    private void handleOnOffXAxis() {
        if (!chartFromFileData.getData().isEmpty()) {
            chartFromFileData.getData()
                    .get(0)
                    .getNode()
                    .setVisible(!cbOnOffXAxis.isSelected());
        }
    }

    /**
     * Обработчик Вкл/Выкл оси y.
     */
    @FXML
    private void handleOnOffYAxis() {
        if (!chartFromFileData.getData().isEmpty()) {
            chartFromFileData.getData()
                    .get(1)
                    .getNode()
                    .setVisible(!cbOnOffYAxis.isSelected());
        }
    }

    /**
     * Обработчик Вкл/Выкл оси z.
     */
    @FXML
    private void handleOnOffZAxis() {
        if (chartFromFileData.getData().size() == 3) {
            chartFromFileData.getData()
                    .get(2)
                    .getNode()
                    .setVisible(!cbOnOffZAxis.isSelected());
        }
    }

    /**
     * Вызывается главным приложением, которое даёт на себя ссылку.
     *
     * @param analyzerApp - ссылка на главное приложение.
     */
    public void setMainApp(AnalyzerApp analyzerApp) {
        this.analyzerApp = analyzerApp;
    }

    /**
     * Отображает данные одного пакета.
     *
     * @param idPackage - индекс пакета.
     */
    private void showDataOnePackage(Integer idPackage) {
        DataPackage dataPackage = dataPackageService.getOnePackageById(idPackage);

        if (!dataPackage.isNull()) {
            lbNumberPackage.setText(String.valueOf(packageList.size()));
            lbPackageNumber.setText(String.valueOf(idPackage));
            lbTimestamp.setText("Метка времени текущего пакета: " +
                    dataPackage.getPackageHeaderData().getDateTime());
            lbSensor.setText(sensorService.findOneByPackage(dataPackage, diagnosticsBlock).getName());
            lbChannelNumber.setText(String.valueOf(
                    dataPackage.getPackageHeaderData()
                            .getDiagnosticData()
                            .getByte2() & 0xf)
            );
            int sampleFrequency = dataPackage.getPackageHeaderData().getDiagnosticData().getByte4() +
                    (dataPackage.getPackageHeaderData().getDiagnosticData().getByte5() << 8);
            lbSampleFrequency.setText(String.valueOf(sampleFrequency));
            int numberOfSamples = arrayOfSamples[dataPackage.getPackageHeaderData().getDiagnosticData().getByte3()];
            lbNumberSamples.setText(String.valueOf(numberOfSamples));
            ratioSampleFreqToNumberSamples = (double) sampleFrequency / numberOfSamples;
            List<Message> listMessageBins = dataPackage.getListPackageDataBins();
            lbNumberBins.setText(String.valueOf(listMessageBins.size()));
            int sensorSensitivity = dataPackage.getPackageHeaderData().getDiagnosticData().getByte6();
            int sensorRange = 0;
            if (sensorSensitivity == arraySensorSensitivity[0]) {
                sensorRange = arraySensorRange[0];
            } else if (sensorSensitivity == arraySensorSensitivity[1]) {
                sensorRange = arraySensorRange[1];
            } else if (sensorSensitivity == arraySensorSensitivity[2]) {
                sensorRange = arraySensorRange[2];
            }
            lbSensorRange.setText(String.valueOf(sensorRange));
            lbSensorSensitivity.setText(String.valueOf(sensorSensitivity));
            if ((dataPackage.getPackageHeaderData().getDiagnosticData().getByte2() & 0x20) == 0x20) {
                lbAdcBit.setText("12");
            } else {
                lbAdcBit.setText("10");
            }
            int u16data1 = dataPackage.getPackageBodyData().getDiagnosticData().getByte4() +
                    (dataPackage.getPackageBodyData().getDiagnosticData().getByte5() << 8);
            int u16data2 = dataPackage.getPackageBodyData().getDiagnosticData().getByte6() +
                    (dataPackage.getPackageBodyData().getDiagnosticData().getByte7() << 8);
            float rmsValue = Float.intBitsToFloat(u16data1 + (u16data2 << 16));
            lbRmsValue.setText(String.format("%.3f", rmsValue));
            lbEngineSpeed.setText(String.valueOf(dataPackage.getEndPackageData().getDiagnosticData().getByte0()));
            lbEngineLoading.setText(String.valueOf(dataPackage.getEndPackageData().getDiagnosticData().getByte1()));

            // вывод графика и заполнение таблицы
            chartPointDataList.clear();
            XYChart.Series<Number, Number> series = drawChartByPackage(dataPackage);
            chartFromFileData.getData().add(series);
            chartPointDataList.addAll(getChartPointsForTable(series));

            lbMaxValue.setText(String.format("%.3f", chartPointDataList.get(0).getAmplitude()));
            lbConstComponent.setText(String.format("%.3f", constComponent));
        }
    }

    /**
     * Рисует график по пакету.
     *
     * @param dataPackage the data package
     * @return the xy chart series
     */
    private XYChart.Series<Number, Number> drawChartByPackage(DataPackage dataPackage) {
        int numberOfSamples = arrayOfSamples[dataPackage.getPackageHeaderData().getDiagnosticData().getByte3()];
        List<Message> listMessageBins = dataPackage.getListPackageDataBins();

        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        int maxScaleFrequency = (int) ((numberOfSamples >> 1) * ratioSampleFreqToNumberSamples);
        for (int i = 0; i < maxScaleFrequency; i++) {
            series.getData().add(new XYChart.Data<>(i, 0));
        }

        for (Message messageBin : listMessageBins) {
            int binNumber = messageBin.getDiagnosticData().getByte2() + (messageBin.getDiagnosticData().getByte3() << 8);
            int u16data1 = messageBin.getDiagnosticData().getByte4() + (messageBin.getDiagnosticData().getByte5() << 8);
            int u16data2 = messageBin.getDiagnosticData().getByte6() + (messageBin.getDiagnosticData().getByte7() << 8);
            float binValue = Float.intBitsToFloat(u16data1 + (u16data2 << 16));
            if (binNumber == 0 && binValue != 0) {
                series.getData().add(new XYChart.Data<>(0, 0));
                constComponent = binValue;
            } else {
                series.getData().add(new XYChart.Data<>(binNumber * ratioSampleFreqToNumberSamples, binValue));
            }
        }

        return series;
    }

    /**
     * Рисует график для метки времени выбранного канала.
     *
     * @param channelNumber номер канала
     * @param timestamp     метка времени
     */
    private void drawChartForChannelTimestamp(int channelNumber, String timestamp) {
        DataPackage dataPackage = dataPackageService.getOnePackageByChannelAndTimestamp(channelNumber, timestamp);
        chartPointDataList.clear();
        XYChart.Series<Number, Number> series = drawChartByPackage(dataPackage);
        chartFromFileData.getData().add(series);
        chartPointDataList.addAll(getChartPointsForTable(series));
        lbSensor.setText(sensorService.findOneByPackage(dataPackage, diagnosticsBlock).getName());
    }

    /**
     * Рисует график для датчика по текущему индексу (<b>currentIndex</b>).
     *
     * @param sensor the sensor
     */
    private void drawChartForSensorByIndex(Sensor sensor) {
        lbPackagesNumberForXAxis.setText(String.valueOf(sensor.getXAxisData().size()));
        lbPackagesNumberForYAxis.setText(String.valueOf(sensor.getYAxisData().size()));
        lbCurrentIndex.setText(String.valueOf(currentIndex));

        XYChart.Series<Number, Number> seriesXAxis = drawChartByPackage(sensor.getXAxisData().get(currentIndex));
        seriesXAxis.setName("Ось X");
        XYChart.Series<Number, Number> seriesYAxis = drawChartByPackage(sensor.getYAxisData().get(currentIndex));
        seriesYAxis.setName("Ось Y");

        chartFromFileData.getData().add(seriesXAxis);
        chartPointDataList.addAll(getChartPointsForTable(seriesXAxis));
        chartFromFileData.getData().add(seriesYAxis);
        yAxisChartPointDataList.addAll(getChartPointsForTable(seriesYAxis));

        if (sensor.getZAxisData() != null) {
            lbPackagesNumberForZAxis.setText(String.valueOf(sensor.getZAxisData().size()));

            XYChart.Series<Number, Number> seriesZAxis = drawChartByPackage(sensor.getZAxisData().get(currentIndex));
            seriesZAxis.setName("Ось Z");

            chartFromFileData.getData().add(seriesZAxis);
            zAxisChartPointDataList.addAll(getChartPointsForTable(seriesZAxis));
        }

        chartFromFileData.setLegendVisible(true);
    }

    /**
     * Показать информационное предупреждение.
     */
    private void showInformationAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Необходима корректировка входного файла!");
        alert.setContentText(message);
        alert.getDialogPane().setMinWidth(750);

        ButtonType openCorrectFile = new ButtonType("Повторно загрузить данные файла");
        ButtonType openFileForFix = new ButtonType("Открыть файл для исправления");
        ButtonType exit = new ButtonType("Выход", ButtonBar.ButtonData.CANCEL_CLOSE);

        // Remove default ButtonTypes
        alert.getButtonTypes().clear();

        alert.getButtonTypes().addAll(openCorrectFile, openFileForFix, exit);

        ButtonBar.setButtonUniformSize(alert.getDialogPane().lookupButton(openFileForFix), false);
        alert.getDialogPane().lookupButton(openFileForFix).addEventFilter(ActionEvent.ACTION, event -> {
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            event.consume();
        });

        Optional<ButtonType> optional = alert.showAndWait();
        optional.ifPresent(buttonType -> {
            if (buttonType.equals(openCorrectFile)) {
                if (excelReader.getExcelPath() != null) {
                    chartFromFileData.getData().clear();
                    getDataSelectedFile();
                }
            } else if (buttonType.equals(exit)) {
                alert.close();
            }
        });
    }

    /**
     * Получить данные выбранного файла.
     */
    private void getDataSelectedFile() {
        try {
            packageList = dataPackageService.getAllPackage();
            channelList = new ChannelServiceImpl(packageList).getChannelList();
            sensorList = sensorService.findAllByBlock(diagnosticsBlock);
            showDataOnePackage(currentIndex);
        } catch (InvalidInputDataException exception) {
            showInformationAlert(exception.getMessage() + ", на листе \"" + excelReader.getSheetName() + "\"!");
        }
    }

    /**
     * Gets chart points for table.
     * Получает точки графика для таблицы.
     *
     * @param series the series
     * @return the chart points for table
     */
    private List<ChartPoint> getChartPointsForTable(XYChart.Series<Number, Number> series) {
        List<ChartPoint> chartPoints = new ArrayList<>();

        for (XYChart.Data<Number, Number> data : series.getData()) {
            double frequency = data.getXValue().doubleValue();
            double amplitude = data.getYValue().doubleValue();
            if (amplitude > 0) {
                chartPoints.add(new ChartPoint(frequency, amplitude));
            }
        }

        // Сортировка по убыванию
        chartPoints.sort(Comparator.comparingDouble(ChartPoint::getAmplitude).reversed());

        return chartPoints;
    }
}
