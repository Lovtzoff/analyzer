package by.ntck.koaims.view;

import by.ntck.koaims.AnalyzerApp;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.awt.*;
import java.io.File;
import java.io.IOException;

import static by.ntck.koaims.constants.Constants.USER_MANUAL;

/**
 * Класс-контроллер для представления корневого макета <b>RootLayoutView.fxml</b>.
 * Корневой макет представляет собой базовый макет приложения, содержащий строку
 * меню и место, где могут быть размещены другие элементы JavaFX.
 *
 * @author Ловцов Алексей
 */
public class RootLayoutController {

    // Ссылка на главное приложение.
    private AnalyzerApp analyzerApp;

    /**
     * Вызывается главным приложением, которое даёт на себя ссылку.
     *
     * @param analyzerApp - ссылка на главное приложение.
     */
    public void setMainApp(AnalyzerApp analyzerApp) {
        this.analyzerApp = analyzerApp;
    }

    /**
     * Открывает руководство пользователя.
     */
    @FXML
    private void handleUserManual() {
        if (Desktop.isDesktopSupported()) {
            try {
                File file = new File(USER_MANUAL);
                Desktop.getDesktop().open(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Открывает диалог о программе.
     */
    @FXML
    private void handleAbout() {
        Hyperlink hyperlink = new Hyperlink("Телеграм-группа");
        hyperlink.setOnAction(event -> analyzerApp.getHostServices().showDocument("https://t.me/+CorYQ8aluNFiZDZi"));

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("КО АиМС\nНТЦК ОАО \"Гомсельмаш\"");

        VBox vBox = new VBox();
        Label label = new Label("Author: Ловцов Алексей");
        vBox.getChildren().addAll(label, hyperlink);

        alert.getDialogPane().contentProperty().set(vBox);
        alert.showAndWait();
    }

    /**
     * Закрывает приложение.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }
}
