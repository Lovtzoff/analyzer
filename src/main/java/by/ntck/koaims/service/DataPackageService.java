package by.ntck.koaims.service;

import by.ntck.koaims.model.DataPackage;

import java.util.List;

/**
 * Интерфейс сервиса для пакетов данных.
 *
 * @author Ловцов Алексей
 */
public interface DataPackageService {

    /**
     * Получить все пакеты.
     *
     * @return список пакетов
     */
    List<DataPackage> getAllPackage();

    /**
     * Получить один пакет по id.
     *
     * @param id идентификатор
     * @return пакет
     */
    DataPackage getOnePackageById(Integer id);

    /**
     * Получить один пакет по каналу и отметке времени.
     *
     * @param channelNumber номер пакета
     * @param timestamp     метка времени
     * @return пакет
     */
    DataPackage getOnePackageByChannelAndTimestamp(int channelNumber, String timestamp);

    /**
     * Получить все пакеты по каналу.
     *
     * @return список пакетов
     */
    List<DataPackage> getAllPackageByChannel(int channelNumber);
}
