package by.ntck.koaims.service;

import by.ntck.koaims.model.Channel;

import java.util.List;

/**
 * Интерфейс сервиса для каналов.
 *
 * @author Ловцов Алексей
 */
public interface ChannelService {

    /**
     * Получить список каналов.
     *
     * @return список каналов
     */
    List<Channel> getChannelList();
}
