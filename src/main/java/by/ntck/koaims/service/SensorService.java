package by.ntck.koaims.service;

import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.model.Sensor;

import java.util.List;

/**
 * Интерфейс с сервисом для датчиков.
 *
 * @author Ловцов Алексей
 */
public interface SensorService {

    /**
     * Найти все датчики.
     *
     * @return the list
     */
    List<Sensor> findAll();

    /**
     * Найти все датчики по блоку диагностики.
     *
     * @param block the block
     * @return the list
     */
    List<Sensor> findAllByBlock(String block);

    /**
     * Найти датчик по пакету данных.
     *
     * @param dataPackage the data package
     * @return the sensor
     */
    Sensor findOneByPackage(DataPackage dataPackage, String block);

    /**
     * Найти датчик по имени.
     *
     * @param name the name
     * @return the sensor
     */
    Sensor findOneByName(String name);
}
