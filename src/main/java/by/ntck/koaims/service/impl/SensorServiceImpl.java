package by.ntck.koaims.service.impl;

import by.ntck.koaims.data.DataReader;
import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.model.Sensor;
import by.ntck.koaims.service.DataPackageService;
import by.ntck.koaims.service.SensorService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static by.ntck.koaims.constants.Constants.SEPARATOR;

/**
 * Реализация интерфейса SensorService.
 *
 * @author Ловцов Алексей
 * @see SensorService
 */
public class SensorServiceImpl implements SensorService {

    /**
     * The Data reader.
     */
    private final DataReader dataReader;
    /**
     * The Data package service.
     */
    private final DataPackageService dataPackageService;

    /**
     * Конструктор.
     *
     * @param dataReader         the data reader
     * @param dataPackageService the data package service
     */
    public SensorServiceImpl(DataReader dataReader, DataPackageService dataPackageService) {
        this.dataReader = dataReader;
        this.dataPackageService = dataPackageService;
    }

    @Override
    public List<Sensor> findAll() {
        List<String> lineList = dataReader.getSensors();
        List<Sensor> sensors = new ArrayList<>(lineList.size());
        for (String line : lineList) {
            String[] array = line.split(SEPARATOR);
            sensors.add(
                    new Sensor(
                            array[0],                                                               // name
                            dataPackageService.getAllPackageByChannel(Integer.parseInt(array[1])),  // xAxisData
                            dataPackageService.getAllPackageByChannel(Integer.parseInt(array[2])),  // yAxisData
                            array[3].isEmpty() ? null : dataPackageService.getAllPackageByChannel(
                                    Integer.parseInt(array[3])
                            ),                                                                      // zAxisData
                            array[4],                                                               // block
                            array[5]                                                                // harvester
                    )
            );
        }
        return sensors;
    }

    @Override
    public List<Sensor> findAllByBlock(String block) {
        return findAll().stream()
                .filter(sensor -> sensor.getBlock().equals(block))
                .collect(Collectors.toList());
    }

    @Override
    public Sensor findOneByPackage(DataPackage dataPackage, String block) {
        for (Sensor sensor : findAllByBlock(block)) {
            if (sensor.getXAxisData().contains(dataPackage) ||
                    sensor.getYAxisData().contains(dataPackage) ||
                    (sensor.getZAxisData() != null && sensor.getZAxisData().contains(dataPackage))) {
                return sensor;
            }
        }
        return new Sensor();
    }

    @Override
    public Sensor findOneByName(String name) {
        return findAll().stream()
                .filter(sensor -> sensor.getName().equals(name))
                .findFirst().orElse(new Sensor());
    }
}
