package by.ntck.koaims.service.impl;

import by.ntck.koaims.data.ExcelReader;
import by.ntck.koaims.data.impl.ExcelReaderImpl;
import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.model.Message;
import by.ntck.koaims.service.DataPackageService;
import by.ntck.koaims.validation.DataValidator;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Реализация интерфейса DataPackageService.
 *
 * @author Ловцов Алексей
 * @see DataPackageService
 */
public class DataPackageServiceImpl implements DataPackageService {

    /**
     * Считыватель данных из Excel.
     */
    private final ExcelReader excelReader;

    /**
     * Конструктор нового сервиса для пакета данных, в который передается считыватель данных.
     *
     * @param excelReader Считыватель данных из Excel
     */
    public DataPackageServiceImpl(ExcelReaderImpl excelReader) {
        this.excelReader = excelReader;
    }

    @Override
    public List<DataPackage> getAllPackage() {
        List<Message> messageList = excelReader.readExcel();
        return new DataValidator(messageList).getValidPackageList();
    }

    @Override
    public DataPackage getOnePackageById(Integer id) {
        return getAllPackage().stream()
                .skip(id)
                .findFirst()
                .orElse(new DataPackage());
    }

    @Override
    public DataPackage getOnePackageByChannelAndTimestamp(int channelNumber, String timestamp) {
        return getAllPackage().stream()
                .filter(dataPackage -> (dataPackage
                        .getPackageHeaderData()
                        .getDiagnosticData()
                        .getByte2() & 0xf) == channelNumber)
                .filter(dataPackage -> dataPackage.getPackageHeaderData().getDateTime().equals(timestamp))
                .findFirst().orElse(new DataPackage());
    }

    @Override
    public List<DataPackage> getAllPackageByChannel(int channelNumber) {
        return getAllPackage().stream()
                .filter(dataPackage -> (dataPackage
                        .getPackageHeaderData()
                        .getDiagnosticData()
                        .getByte2() & 0xf) == channelNumber)
                .collect(Collectors.toList());
    }
}
