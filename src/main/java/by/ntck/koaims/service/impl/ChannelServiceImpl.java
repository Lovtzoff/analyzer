package by.ntck.koaims.service.impl;

import by.ntck.koaims.model.Channel;
import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.service.ChannelService;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация интерфейса ChannelService.
 *
 * @author Ловцов Алексей
 * @see ChannelService
 */
public class ChannelServiceImpl implements ChannelService {

    /**
     * Список пакетов.
     */
    private final List<DataPackage> packageList;

    /**
     * Конструктор.
     *
     * @param packageList входной список пакетов
     */
    public ChannelServiceImpl(List<DataPackage> packageList) {
        this.packageList = packageList;
    }

    @Override
    public List<Channel> getChannelList() {
        List<Channel> channelList = new ArrayList<>();
        for (Integer channel : getChannels()) {
            channelList.add(new Channel(channel, getTimestampsList(channel)));
        }
        return channelList;
    }

    /**
     * Получить список доступных каналов.
     *
     * @return список каналов
     */
    private List<Integer> getChannels() {
        List<Integer> channels = new ArrayList<>();
        for (DataPackage dataPackage : packageList) {
            if (!channels.contains(dataPackage.getPackageHeaderData().getDiagnosticData().getByte2() & 0xf)) {
                channels.add(dataPackage.getPackageHeaderData().getDiagnosticData().getByte2() & 0xf);
            }
        }
        return channels;
    }

    /**
     * Получить список меток времени.
     *
     * @param channelNumber номер канала
     * @return список меток времени
     */
    private List<String> getTimestampsList(int channelNumber) {
        List<String> timestampsList = new ArrayList<>();
        for (DataPackage dataPackage : packageList) {
            if (channelNumber == (dataPackage.getPackageHeaderData().getDiagnosticData().getByte2() & 0xf)) {
                timestampsList.add(dataPackage.getPackageHeaderData().getDateTime());
            }
        }
        return timestampsList;
    }
}
