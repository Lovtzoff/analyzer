package by.ntck.koaims.validation;

import by.ntck.koaims.exception.InvalidInputDataException;
import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.model.DiagnosticData;
import by.ntck.koaims.model.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static by.ntck.koaims.constants.Constants.BlockGps.SMART;
import static by.ntck.koaims.constants.Constants.*;

/**
 * Класс, содержащий методы для проверки входных данных.
 *
 * @author Ловцов Алексей
 */
public class DataValidator {

    /**
     * Входной список сообщений.
     */
    private final List<Message> messageList;

    /**
     * Создает новый валидатор данных.
     *
     * @param messageList входной список сообщений
     */
    public DataValidator(List<Message> messageList) {
        this.messageList = messageList;
    }

    /**
     * Получить действительный список пакетов.
     *
     * @return действительный список данных пакетов
     */
    public List<DataPackage> getValidPackageList() {
        List<List<Message>> invalidPackageList = getInvalidPackageList();
        return invalidPackageList.stream()
                .map(this::getValidPackageMessages)
                .map(validPackageMessages ->
                {
                    int size = validPackageMessages.size();
                    int endIndex = (size > DEFAULT_DATA_PACKET_SIZE) ? (size - 1) : DEFAULT_END_INDEX;
                    return new DataPackage(
                            validPackageMessages.get(DEFAULT_HEADER_INDEX),
                            validPackageMessages.get(DEFAULT_BODY_INDEX),
                            validPackageMessages.subList(DEFAULT_FIRST_BIN_INDEX, endIndex),
                            validPackageMessages.get(endIndex));
                })
                .collect(Collectors.toList());
    }

    /**
     * Получить индексы заголовков пакетов.
     *
     * @return индексы заголовков пакетов
     */
    private List<Integer> getPacketHeaderIndexes() {
        List<Integer> headerIndexList = messageList.stream()
                .filter(message -> (message.getDiagnosticData().getByte0() + (message.getDiagnosticData().getByte1() << 8)) == SYNC_WORD)
                .map(messageList::indexOf)
                .collect(Collectors.toList());
        if (currentBlockGps.equals(SMART)) {
            for (int i = 0; i < headerIndexList.size() - 1; i++) {
                while (headerIndexList.get(i) > (headerIndexList.get(i + 1) - DEFAULT_DATA_PACKET_SIZE)) {
                    headerIndexList.remove(i + 1);
                    if (i == (headerIndexList.size() - 1)) {
                        break;
                    }
                }
            }
        }
        return headerIndexList;
    }

    /**
     * Получить индексы концов пакетов.
     *
     * @return индексы концов пакетов
     */
    private List<Integer> getPacketEndIndexes() {
        List<Integer> endIndexList = messageList.stream()
                .filter(message -> (message.getDiagnosticData().getByte2() + (message.getDiagnosticData().getByte3() << 8)) == SYNC_WORD)
                .map(messageList::indexOf)
                .collect(Collectors.toList());
        if (currentBlockGps.equals(SMART)) {
            for (int i = 0; i < endIndexList.size() - 1; i++) {
                while (endIndexList.get(i) > (endIndexList.get(i + 1) - DEFAULT_DATA_PACKET_SIZE)) {
                    endIndexList.remove(i);
                    if (i == (endIndexList.size() - 1)) {
                        break;
                    }
                }
            }
        }
        return endIndexList;
    }

    /**
     * Получить неверный список пакетов.
     *
     * @return неверный список пакетов
     */
    private List<List<Message>> getInvalidPackageList() {
        List<Integer> headerIndexList = getPacketHeaderIndexes();
        List<Integer> endIndexList = getPacketEndIndexes();
        int size = Math.min(headerIndexList.size(), endIndexList.size());

        if (currentBlockGps.equals(SMART)) {
            int counter = 1;
            for (int i = 0; i < size - 1; i++) {
                int a = (getPacketEndIndexes().get(i) + 1);
                int b = getPacketHeaderIndexes().get(i + 1);
                int c = getPacketEndIndexes().get(i);
                int d = getPacketHeaderIndexes().get(i) + DEFAULT_DATA_PACKET_SIZE * 10;
                if ((a < b) && (counter < 9)) {
                    throw new InvalidInputDataException(
                            "Некорректные входные данные! Проверьте данные сообщений (№" +
                                    (getPacketEndIndexes().get(i) + 2) + " - " +
                                    getPacketHeaderIndexes().get(i + 1) + ")");
                } else if (c > d && (counter < 9)) {
                    throw new InvalidInputDataException(
                            "Некорректные входные данные! Присутствуют неполные пакеты (Сообщения №" +
                                    (getPacketHeaderIndexes().get(i) + 1) + " - " +
                                    (getPacketEndIndexes().get(i) + 1) + ")");
                } else {
                    counter = (counter != 9) ? ++counter : 1;
                }
            }
        }

        if (getPacketHeaderIndexes().size() != getPacketEndIndexes().size()) {
            throw new InvalidInputDataException(
                    "Некорректные входные данные! " +
                            "Проверьте целостность пакетов.\n" +
                            "Количество заголовков не совпадает с количеством концов пакетов: \n" +
                            "кол-во заголовков = " + getPacketHeaderIndexes().size() + ", " +
                            "кол-во концов = " + getPacketEndIndexes().size());
        }

        return IntStream.range(0, size)
                .mapToObj(i -> messageList.subList(headerIndexList.get(i), endIndexList.get(i) + 1))
                .collect(Collectors.toCollection(() -> new ArrayList<>(size)));
    }

    /**
     * Получить действительные сообщения пакета.
     *
     * @param invalidPackageMessages недействительные сообщения пакета
     * @return действительные сообщения пакета
     */
    private List<Message> getValidPackageMessages(List<Message> invalidPackageMessages) {
        List<Message> packageMessages = invalidPackageMessages.stream()
                .filter(message -> !message.getDiagnosticData().isNull())
                .collect(Collectors.toList());

        for (int i = 0; i < packageMessages.size(); i++) {
            for (int j = 0; j < packageMessages.size(); j++) {
                if (i == j) {
                    continue;
                }
                // Удаление дубликатов
                while (packageMessages.get(i).getDiagnosticData().equals(packageMessages.get(j).getDiagnosticData())) {
                    if ((packageMessages.get(i).getDiagnosticData().getByte2() +
                            (packageMessages.get(i).getDiagnosticData().getByte3() << 8)) == SYNC_WORD) {
                        packageMessages.remove(i);
                    } else {
                        packageMessages.remove(j);

                    }
                    if (j == packageMessages.size()) {
                        break;
                    }
                }
            }
        }

        int numberBins = packageMessages.get(0).getDiagnosticData().getByte7();
        int dataPacketSize = (numberBins > DEFAULT_DATA_PACKET_SIZE) ? (numberBins + 3) : DEFAULT_DATA_PACKET_SIZE;

        if (packageMessages.size() > dataPacketSize) {
            if ((packageMessages.get(packageMessages.size() - 1).getDiagnosticData().getByte0() +
                    (packageMessages.get(packageMessages.size() - 1).getDiagnosticData().getByte1() << 8)) == SYNC_WORD) {
                packageMessages.remove(packageMessages.size() - 1);
            }
            if ((packageMessages.get(DEFAULT_BODY_INDEX).getDiagnosticData().getByte2() +
                    (packageMessages.get(DEFAULT_BODY_INDEX).getDiagnosticData().getByte3() << 8)) == SYNC_WORD) {
                packageMessages.remove(DEFAULT_BODY_INDEX);
            }
        } else if (packageMessages.size() < dataPacketSize) {
            while (packageMessages.size() != dataPacketSize) {
                packageMessages.add(
                        packageMessages.size() - 1,
                        new Message(0, null, new DiagnosticData())
                );
            }
        }

        return packageMessages;
    }
}
