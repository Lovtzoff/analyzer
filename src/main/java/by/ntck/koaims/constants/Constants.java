package by.ntck.koaims.constants;

/**
 * Класс, в котором хранятся постоянные переменные: пути и др.
 *
 * @author Ловцов Алексей
 */
public class Constants {

    public static final String DATA_DIR = "data";
    public static final String SHEET_OBJECT_CHARACTERISTICS = "Характеристики объекта";
    public static final String SHEET_NAME_DEVICE = "Устройство мониторинга";
    public static final String SHEET_NAME_FOR_FIRST_BLOCK = "18FF0B85";
    public static final String SHEET_NAME_FOR_SECOND_BLOCK = "18FF0B86";
    public static final int SYNC_WORD = 65535;

    public static final String VIEW_DIR = "/by/ntck/koaims/view/";
    public static final String VIEW_ROOT = VIEW_DIR + "RootLayoutView.fxml";
    public static final String VIEW_CHART = VIEW_DIR + "AnalysisChartView.fxml";

    public static final String STYLE_DIR = "/by/ntck/koaims/style/";
    public static final String STYLE_CHART = STYLE_DIR + "chart.css";

    public static final int DEFAULT_DATA_PACKET_SIZE = 13;
    public static final int DEFAULT_HEADER_INDEX = 0;
    public static final int DEFAULT_BODY_INDEX = 1;
    public static final int DEFAULT_FIRST_BIN_INDEX = 2;
    public static final int DEFAULT_END_INDEX = 12;

    public static BlockGps currentBlockGps;

    public enum BlockGps {
        SMART,
        VEGA
    }

    public static final String USER_MANUAL = "manual/user_manual_analyzer.pdf";

    public static final int[] arrayOfSamples = {64, 128, 256, 512, 1024, 2048, 4096, 8192};
    public static final int[] arraySensorSensitivity = {220, 57, 6};
    public static final int[] arraySensorRange = {6, 16, 200};

    public static final String SENSOR_LIST = "sensors/data/sensor.list";
    public static final String SEPARATOR  = ";";
    public static String harvesterModel;
    public static String diagnosticsBlock;
    public static final String BLOCK_A1 = "A1";
    public static final String BLOCK_A2 = "A2";

    // Test
    public static final String TEST_DATA_1624 = "/excel/testData1624.xlsx";
}
