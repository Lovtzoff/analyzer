package by.ntck.koaims.util.test;

import by.ntck.koaims.model.Sensor;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, содержащий утилиты для класса Sensor.
 *
 * @author Ловцов Алексей
 */
public class SensorUtils {

    /**
     * Создать список датчиков для 1624.
     *
     * @return список датчиков
     */
    public static List<Sensor> createSensorList1624() {
        List<Sensor> sensorList = new ArrayList<>(3);
        sensorList.add(
                new Sensor(
                        "Датчик вибрации соломоизмельчителя слева (B1)",
                        null,
                        null,
                        null,
                        "A1",
                        "КЗС-1624-1"
                )
        );
        sensorList.add(
                new Sensor(
                        "Датчик вибрации соломоизмельчителя справа (B2)",
                        null,
                        null,
                        null,
                        "A1",
                        "КЗС-1624-1"
                )
        );
        sensorList.add(
                new Sensor(
                        "Датчик вибрации наклонной камеры (B3)",
                        null,
                        null,
                        null,
                        "A1",
                        "КЗС-1624-1"
                )
        );
        return sensorList;
    }
}
