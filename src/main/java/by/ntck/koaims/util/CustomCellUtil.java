package by.ntck.koaims.util;

import by.ntck.koaims.model.ChartPoint;
import javafx.scene.control.TableCell;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.text.NumberFormat;


/**
 * The type Custom cell util.
 * Утилитный класс CustomCell.
 *
 * @author Ловцов Алексей
 */
@AllArgsConstructor
@Getter
public class CustomCellUtil extends TableCell<ChartPoint, Double> {

    /**
     * The format.
     * Формат.
     */
    private NumberFormat format;

    @Override
    protected void updateItem(Double item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
        } else {
            setText(format.format(item));
        }
    }
}
