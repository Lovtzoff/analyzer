package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Класс-модель для сообщения с полями <b>index</b>, <b>dateTime</b> и <b>data</b>.
 *
 * @author Ловцов Алексей
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message {

    /**
     * The Index.
     */
    private int index;
    /**
     * Метка времени (дата/время)
     */
    private String dateTime;
    /**
     * The Data.
     */
    private DiagnosticData diagnosticData;
}
