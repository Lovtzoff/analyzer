package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Класс-модель для канала (Channel).
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Channel {

    /**
     * Номер канала.
     */
    private int channelNumber;
    /**
     * Список меток времени.
     */
    private List<String> timestamp;
}
