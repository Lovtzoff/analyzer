package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Класс-модель для точки графика.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChartPoint {

    /**
     * Частота.
     */
    private Double frequency;
    /**
     * Амплитуда.
     */
    private Double amplitude;
}
