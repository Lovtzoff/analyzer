package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.stream.Stream;

/**
 * Класс-модель для данных диагностики с полями <b>byte0</b>, <b>byte1</b>, <b>byte2</b>, <b>byte3</b>,
 * <b>byte4</b>, <b>byte5</b>, <b>byte6</b> и <b>byte7</b>.
 *
 * @author Ловцов Алексей
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DiagnosticData {

    /**
     * The Byte 0.
     */
    private int byte0;
    /**
     * The Byte 1.
     */
    private int byte1;
    /**
     * The Byte 2.
     */
    private int byte2;
    /**
     * The Byte 3.
     */
    private int byte3;
    /**
     * The Byte 4.
     */
    private int byte4;
    /**
     * The Byte 5.
     */
    private int byte5;
    /**
     * The Byte 6.
     */
    private int byte6;
    /**
     * The Byte 7.
     */
    private int byte7;

    public boolean isNull() {
        return Stream.of(getByte0(),
                        getByte1(),
                        getByte2(),
                        getByte3(),
                        getByte4(),
                        getByte5(),
                        getByte6(),
                        getByte7())
                .allMatch(obj -> obj.equals(0));
    }
}
