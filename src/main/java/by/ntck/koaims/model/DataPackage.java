package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Класс-модель для пакета данных с полями <b>packageHeaderData</b>, <b>packageBodyData</b>,
 * <b>listPackageDataBins</b> и <b>endPackageData</b>.
 *
 * @author Ловцов Алексей
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DataPackage {

    /**
     * Заголовок пакета.
     */
    private Message packageHeaderData;
    /**
     * Тело пакета.
     */
    private Message packageBodyData;
    /**
     * Список данных "бинов".
     */
    private List<Message> listPackageDataBins;
    /**
     * Конец пакета.
     */
    private Message endPackageData;

    public boolean isNull() {
        return Stream.of(getPackageHeaderData(),
                        getPackageBodyData(),
                        getListPackageDataBins(),
                        getEndPackageData())
                .allMatch(Objects::isNull);
    }
}
