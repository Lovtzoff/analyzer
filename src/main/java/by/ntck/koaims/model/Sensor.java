package by.ntck.koaims.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Класс-модель для датчика с полями <b>name</b>, <b>xAxisData</b>, <b>yAxisData</b>, <b>zAxisData</b>,
 * <b>block</b> и <b>harvester</b>.
 *
 * @author Ловцов Алексей
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Sensor {

    /**
     * Имя датчика.
     */
    private String name;
    /**
     * Данные оси X.
     */
    private List<DataPackage> xAxisData;
    /**
     * Данные оси Y.
     */
    private List<DataPackage> yAxisData;
    /**
     * Данные оси Z.
     */
    private List<DataPackage> zAxisData;
    /**
     * Номер блока диагностики.
     */
    private String block;
    /**
     * Модель комбайна.
     */
    private String harvester;
}
