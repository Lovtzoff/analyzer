package by.ntck.koaims;

import by.ntck.koaims.view.AnalysisChartController;
import by.ntck.koaims.view.RootLayoutController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lombok.Getter;

import java.io.IOException;
import java.util.Objects;

import static by.ntck.koaims.constants.Constants.*;

/**
 * Основной класс приложения.
 */
public class AnalyzerApp extends Application {

    @Getter
    private Stage primaryStage;
    private BorderPane rootLayout;

    /**
     * Конструктор
     */
    public AnalyzerApp() {
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Analyzer");

        initRootLayout();

        showAnalysisChart();
    }

    /**
     * Инициализация корневого макета
     */
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AnalyzerApp.class.getResource(VIEW_ROOT));
            rootLayout = loader.load();

            // Даём контроллеру доступ к главному приложению.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            Scene scene = new Scene(rootLayout);
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource(STYLE_CHART)).toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Показывает графический анализ данных внутри корневого макета
     */
    public void showAnalysisChart() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AnalyzerApp.class.getResource(VIEW_CHART));
            AnchorPane analysisChart = loader.load();

            rootLayout.setCenter(analysisChart);

            // Даём контроллеру доступ к главному приложению.
            AnalysisChartController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Точка запуска приложения.
     *
     * @param args массив входных параметров
     */
    public static void main(String[] args) {
        launch(args);
    }
}
