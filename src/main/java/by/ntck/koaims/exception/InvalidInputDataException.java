package by.ntck.koaims.exception;

/**
 * Сигналы о том, что произошло какое-то исключение, связанное с входными данными.
 *
 * @author Ловцов Алексей
 */
public class InvalidInputDataException extends RuntimeException {

    /**
     * Создает новое исключение недопустимых входных данных.
     */
    public InvalidInputDataException() {
        super("Некорректные входные данные!");
    }

    /**
     * Создает новое исключение недопустимых входных данных, с заданным сообщением.
     *
     * @param message сообщение
     */
    public InvalidInputDataException(String message) {
        super(message);
    }
}
