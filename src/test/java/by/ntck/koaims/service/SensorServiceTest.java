package by.ntck.koaims.service;

import by.ntck.koaims.data.DataReader;
import by.ntck.koaims.data.impl.DataReaderImpl;
import by.ntck.koaims.data.impl.ExcelReaderImpl;
import by.ntck.koaims.model.DataPackage;
import by.ntck.koaims.model.Sensor;
import by.ntck.koaims.service.impl.DataPackageServiceImpl;
import by.ntck.koaims.service.impl.SensorServiceImpl;
import by.ntck.koaims.util.test.SensorUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static by.ntck.koaims.constants.Constants.*;

class SensorServiceTest {

    static List<Sensor> sensorList;
    static ExcelReaderImpl excelReader = new ExcelReaderImpl();
    DataPackageService dataPackageService = new DataPackageServiceImpl(excelReader);
    DataReader dataReader = new DataReaderImpl();
    SensorService sensorService = new SensorServiceImpl(dataReader, dataPackageService);
    static File testFile;

    @BeforeAll
    static void generateSourceData() {
        sensorList = SensorUtils.createSensorList1624();

        try {
            testFile = new File(Objects.requireNonNull(SensorServiceTest.class.getResource(TEST_DATA_1624)).toURI());
            excelReader.setExcelPath(testFile.getAbsolutePath());
            excelReader.readHarvesterModel();
            excelReader.readGpsDeviceType();
            excelReader.setSheetName(SHEET_NAME_FOR_FIRST_BLOCK);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @AfterAll
    static void deleteSourceData() {
        sensorList = null;
        testFile = null;
        excelReader = null;
    }

    @Test
    void findAll() {
        List<Sensor> sensors = sensorService.findAll();
        Assertions.assertEquals(sensorList.size(), sensors.size());
        IntStream.range(0, sensorList.size())
                .forEach(i -> {
                    Assertions.assertEquals(sensorList.get(i).getName(), sensors.get(i).getName());
                    Assertions.assertEquals(sensorList.get(i).getBlock(), sensors.get(i).getBlock());
                    Assertions.assertEquals(sensorList.get(i).getHarvester(), sensors.get(i).getHarvester());
                });
    }

    @Test
    void findAllByBlock(){
        String block = "A1";
        List<Sensor> sensors = sensorService.findAllByBlock(block);
        Assertions.assertEquals(sensorList.size(), sensors.size());
        IntStream.range(0, sensorList.size())
                .forEach(i -> {
                    Assertions.assertEquals(sensorList.get(i).getName(), sensors.get(i).getName());
                    Assertions.assertEquals(sensorList.get(i).getBlock(), sensors.get(i).getBlock());
                    Assertions.assertEquals(sensorList.get(i).getHarvester(), sensors.get(i).getHarvester());
                });
    }

    @Test
    void findOneByPackage() {
        List<DataPackage> packageList = dataPackageService.getAllPackageByChannel(0);
        IntStream.range(0, packageList.size())
                .forEach(i -> Assertions.assertEquals(
                        sensorList.get(0).getName(),
                        sensorService.findOneByPackage(packageList.get(i), BLOCK_A1).getName()
                ));
    }

    @Test
    void findOneByName() {
        String nameSensor = "Датчик вибрации соломоизмельчителя слева (B1)";
        Assertions.assertEquals(sensorList.get(0).getName(), sensorService.findOneByName(nameSensor).getName());
    }
}